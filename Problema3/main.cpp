#include <iostream>
#include <string>


using namespace std;

bool comparacion(string word1,string word2){
    int limite = word1.length();        //Pone el límite de letras por las que pasará el contador
    bool resultado=true;
    char letter1,letter2;
    for (int i=0;i < limite; i++){            //Recorre hasta la ultima letra, una a una
        letter1=word1[i];
        letter2=word2[i];
        if (letter1 != letter2){    //Si las letras de la misma posicion son diferentes, rompe el ciclo y se da como diferente
            resultado=false;
            break;
        }

    }
    return resultado;
}
void complemento(string palabra1, string palabra2){
    bool resultado=false;
    if (palabra1.length()==palabra2.length())  //Verifica que la longitud de las cadenas sean iguales
    {
        resultado=comparacion(palabra1,palabra2);    //Va a la funcion que los compara letra a letra
        if (resultado == true)
            cout << "Las cadenas de caracteres son iguales." << endl;
        else
            cout <<"Las cadenas de caracteres son diferentes."<<endl;
    }
    else                            //Si son de tamaños diferentes, inmediatamente sale como Diferente
        cout <<"Las cadenas de caracteres son diferentes."<<endl;

}
void prueba(){
    char prueba;
    cout << "¿Desea realizar el programa de prueba? (y)"<< endl;
    cin >> prueba;
    //cin.ignore();
    if (prueba == 'y' || prueba == 'Y'){
        string palabra1="Hola Mundo!",palabra2="Hola Mundo!";

        cout <<"Prueba 1: "<<endl;
        cout <<"Cadena 1: Hola Mundo!"<<endl;
        cout << "Cadena 2: Hola Mundo!"<<endl;

        complemento(palabra1,palabra2);

        cout <<"Prueba 2: "<<endl;
        cout <<"Palabra 1: Informatica 1"<<endl;
        palabra1="Informatica 1";
        cout << "Palabra 2: Calculo"<<endl;
        palabra2="Calculo";

        complemento(palabra1,palabra2);
        cout <<"Se procede al uso normal del codigo..."<<endl;
    }
    else
        cout <<"Se procede al uso normal del codigo..."<<endl;

}

int main()
{
    /* Este programa recibe dos cadenas de caracteres y compara si son iguales o diferentes, tomando en cuenta
 su tamaño; siendo iguales en tamaño, se pasa a verificar letra por letra si es igual a la de la misma posicion
en la otra cadena*/
    string palabra1,palabra2;

    prueba();
    cin.ignore();
    cout << "Ingrese una cadena de caracteres: " << endl;
    getline (cin,palabra1);
    cout << "Ingrese una segunda cadena de caracteres: " << endl;
    getline (cin,palabra2);

    complemento(palabra1,palabra2);  //Realiza todo el proceso en Funciones

    return 0;

}
